//OA Tip SIM
//Tylor Emmett
//02/20/20
//This SIM serves to adjust an OA tip to the correct employee after the check is closed.

//POS Config values
var OAEmployee : N4 = 9997
var TipTrackingTotal : A2 = "20"
var TipSVCObjNum : N4 = 501

//Database access config values
var DB_USER : A10 = ""
var DB_PASSWORD : A10 = ""

//Buffers for SQL Queries and results
var SqlSelect : A500
var SqlResult : A255

//For storing relevant tip details
var TipAmt : $10
var TipSeq : N10
var TipTmedSeq : N10
var CMTipSvcSeq : N10
var CMTipTmedSeq : N10
var TransSeq : N10
var DtlSeq : N10
var BusinessDate : A40

//For logging
var DebugLogMode : A1 = "T"
var logMessage : A500
var DebugLogMessage : A500

event FINAL_TENDER
	if ( @ORDERTYPE = 6 )
		format logMessage as "OA Order closed. Check Sequence: ", @CKSEQNUM
		call Log(logMessage)
		call GetTipInfo
		if ( TipAmt > 0.00 )
			format logMessage as "Tip: ", TipAmt
			call Log(logMessage)
			call AdjustTips
		else
			format logMessage as "No Tip Found"
			call Log(logMessage)
			exitcontinue
		endif
	endif
endevent

sub AdjustTips
	var OAEmpSeq : N10
	var OAReceptacle : N10
	var OAShift: N10
	var EmpSeq : N10
	var EmpShift : N10
	var EmpReceptacle : N10
	var dbHandle : N10
	
	call DB_Open(dbHandle)
	//Get the business date from micros.rest_status
	format sqlSelect as "select business_date from micros.rest_status"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	BusinessDate = sqlResult
	call DebugLog(sqlResult)

	//Get Tip Tmed Seq
	format sqlSelect as "select tips_paid_tmed_seq from micros.dsvc_def where obj_num = ",TipSVCObjNum,";"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	TipTmedSeq = sqlResult
	call DebugLog(sqlResult)
	
	//Get CM Item seq for Tip SVC
	format sqlSelect as "select cm_item_seq from micros.cm_item_def where cm_item_type = 2 and res_item_id = ", TipSeq, ";"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	CMTipSvcSeq = sqlResult
	call DebugLog(sqlResult)
	
	//Get CM Item seq for Tip TMED
	format sqlSelect as "select cm_item_seq from micros.cm_item_def where cm_item_type = 0 and res_item_id = ", TipTmedSeq, ";"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	CMTipTmedSeq = sqlResult
	call DebugLog(sqlResult)
	
	//Get Employee Seqs
	format sqlSelect as "select emp_seq from micros.emp_def where obj_num = ", OAEmployee, ";"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	OAEmpSeq = sqlResult
	call DebugLog(sqlResult)
	
	format sqlSelect as "select emp_seq from micros.emp_def where obj_num = ", @TREMP, ";"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	EmpSeq = sqlResult
	call DebugLog(sqlResult)

	//Get shift seq for OA Employee
	format sqlSelect as "select shift_seq from micros.shift_emp_dtl where emp_seq = ", OAEmpSeq, " and shift_end_time is null;"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	OAShift = sqlResult
	call DebugLog(sqlResult)
	
	//Get shift seq for Trans Employee
	format sqlSelect as "select shift_seq from micros.shift_emp_dtl where emp_seq = ", EmpSeq , " and shift_end_time is null;"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	EmpShift = sqlResult
	call DebugLog(sqlResult)
	
	//Get CM receptacle seq for OA Employee
	format sqlSelect as "select receptacle_seq from micros.cm_employee_receptacle_assignment_dtl where employee_assigned = 'T' and employee_seq = ", OAEmpSeq, ";"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	OAReceptacle = sqlResult
	call DebugLog(sqlResult)
	
	//Get CM receptacle seq for Trans Employee
	format sqlSelect as "select receptacle_seq from micros.cm_employee_receptacle_assignment_dtl where employee_assigned = 'T' and employee_seq = ", EmpSeq , ";"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	EmpReceptacle = sqlResult
	call DebugLog(sqlResult)

	//If no receptacle, create one and assign it.
	if(EmpReceptacle = 0)
		format logMessage as "Employee does not yet have a server bank. Creating one."
		call Log(logMessage)
		
		format sqlSelect as "insert into micros.cm_receptacle_dtl (receptacle_seq, receptacle_type, open_business_date, open_timestamp, receptacle_state, receptacle_name, starting_amount, cm_accounting_method, countsheet_seq, open_timestamp_loc) values ((select max(receptacle_seq) + 1 from micros.cm_receptacle_dtl), 0, (select business_date from micros.rest_status), now(), 0, 'Carhop', 0.00, 1, 3, now())"
		call DebugLog(sqlSelect)
		call DB_Execute(dbHandle, sqlSelect)
		format sqlSelect as "insert into micros.cm_employee_receptacle_assignment_dtl values ((select max(receptacle_seq) from micros.cm_receptacle_dtl),",EmpSeq,",'T')"
		call DebugLog(sqlSelect)
		call DB_Execute(dbHandle, sqlSelect)	
	
		//Get CM receptacle seq for Trans Employee
		format sqlSelect as "select receptacle_seq from micros.cm_employee_receptacle_assignment_dtl where employee_assigned = 'T' and employee_seq = ", EmpSeq , ";"
		call DebugLog(sqlSelect)
		call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
		EmpReceptacle = sqlResult	
		call DebugLog(sqlResult)
		
		format logMessage as "Server Bank ", EmpReceptacle, " created."
		call Log(logMessage)
	endif
	
	//Move tip from OA to Emp in CM
	format sqlSelect as "update micros.cm_transaction_dtl set transaction_receptacle_seq = ",EmpReceptacle, ", transaction_employee_seq = ", EmpSeq ," where pos_transaction_id = ", TransSeq, " and cm_item_seq = ", CMTipSvcSeq, ";"
	call DebugLog(sqlSelect)
	call DB_Execute(dbHandle, sqlSelect)
		
	format sqlSelect as "update micros.cm_transaction_dtl set transaction_receptacle_seq = ",EmpReceptacle, ", transaction_employee_seq = ", EmpSeq ," where pos_transaction_id = ", TransSeq, " and cm_item_seq = ", CMTipTmedSeq, ";"
	call DebugLog(sqlSelect)
	call DB_Execute(dbHandle, sqlSelect)
	
	format logMessage as "Moved CM Tip Transactions to Server Bank ", EmpReceptacle, " for POS Transaction #  ", TransSeq
	call DebugLog(sqlSelect)
	call Log(logMessage)

	//Update tip in Tracking Totals
	format sqlSelect as "update micros.shift_emp_trk_ttl_temp set ttl = ttl - ",TipAmt,", cnt = cnt - 1 where trk_ttl_seq = ",TipTrackingTotal," and emp_seq = ",OAEmpSeq," and shift_seq = ",OAShift,";"
	call DebugLog(sqlSelect)
	call DB_Execute(dbHandle, sqlSelect)
	
	format sqlSelect as "update micros.shift_emp_trk_ttl set trk_ttl_",TipTrackingTotal," = trk_ttl_",TipTrackingTotal," - ",TipAmt,", trk_cnt_",TipTrackingTotal," = trk_cnt_",TipTrackingTotal," - 1 where emp_seq = ",OAEmpSeq," and shift_seq = ",OAShift,";"
	call DebugLog(sqlSelect)
	call DB_Execute(dbHandle, sqlSelect)
	
	format logMessage as "Removed Tip from API Employee"
	call Log(logMessage)

	var bAlreadyHasTips : N5
	format sqlSelect as "select count(*) from micros.shift_emp_trk_ttl_temp where emp_seq = ",EmpSeq," and shift_seq = ",EmpShift," and trk_ttl_seq = ",TipTrackingTotal,";" 
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	bAlreadyHasTips = sqlResult
	call DebugLog(sqlResult)

	//If no tracking total currently exists
	if(bAlreadyHasTips = 0)
		format logMessage as "Employee does not have a tip tracking total entry. Creating one."
		call Log(logMessage)
		
		format sqlSelect as "insert into micros.shift_emp_trk_ttl_temp values (", EmpSeq ,",",EmpShift,",",17,",",TipTrackingTotal,",",1,",",TipAmt,",",4,");"
		call DebugLog(sqlSelect)
		call DB_Execute(dbHandle, sqlSelect)
	else
	//If a tracking total already exists
		format sqlSelect as "update micros.shift_emp_trk_ttl_temp set ttl = ttl + ",TipAmt,", cnt = cnt + 1 where trk_ttl_seq = ",TipTrackingTotal," and emp_seq = ",EmpSeq," and shift_seq = ",EmpShift,";"
		call DebugLog(sqlSelect)
		call DB_Execute(dbHandle, sqlSelect)
	endif
	
	format sqlSelect as "update micros.shift_emp_trk_ttl set trk_ttl_",TipTrackingTotal," = trk_ttl_",TipTrackingTotal," + ",TipAmt,", trk_cnt_",TipTrackingTotal," = trk_cnt_",TipTrackingTotal," + 1 where emp_seq = ", EmpSeq ," and shift_seq = ", EmpShift ,";"
	call DebugLog(sqlSelect)
	call DB_Execute(dbHandle, sqlSelect)
	
	format logMessage as "Added Tip to Employee # ", @TREMP
	call Log(logMessage)
	
	//Post totals
	format sqlSelect as "call micros.sp_PostAll();"
	call DebugLog(sqlSelect)
	call DB_Execute(dbHandle, sqlSelect)
	
	call DB_Close(dbHandle)
endsub

sub GetTipInfo
	var dbHandle : N10
	
	call DB_Open(dbHandle)

	//Get the tip SVC's sequence number
	format sqlSelect as "select dsvc_seq from micros.dsvc_def where obj_num = ", TipSVCObjNum , ";"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	TipSeq = sqlResult
	call DebugLog(sqlResult)
	
	//Find the transaction and DTL sequence the service charge came in on by check sequence and SVC sequence
	format sqlSelect as "select trans_seq, dtl_seq from micros.dsvc_dtl where dsvc_seq = ", TipSeq , " and trans_seq in (select trans_seq from micros.trans_dtl where chk_seq = ", @CKSEQNUM, ");"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	split sqlResult, ";", TransSeq, DtlSeq
	call DebugLog(sqlResult)
	
	//Get the tip amount from the dtl table
	format sqlSelect as "select rpt_ttl from micros.dtl where trans_seq = " , TransSeq, " and dtl_seq = ", DtlSeq, ";"
	call DebugLog(sqlSelect)
	call DB_GetSingleRowResult(dbHandle, sqlSelect, sqlResult)
	TipAmt = sqlResult
	call DebugLog(sqlResult)
endsub

//Database Access
sub DB_Open(ref _handle)

	_handle = 0
	dllLoad _handle, "MDSSysUtilsProxy.dll"

	if (dbHandle = 0)
		exitWithError "Unable to Load MDSSysUtilsProxy.dll"
	endif
   
	var connString: A256
	format connString as "ODBC;UID=", DB_USER, ";PWD=", DB_PASSWORD
	dllCall_cdecl _handle, sqlInitConnection("micros", connString, "")

endsub 

//-5-//
sub DB_Execute(ref _handle, ref _statement)

	dllCall_cdecl _handle, sqlExecuteQuery(ref _statement)

endsub 

//-6-//
Sub DB_Close(ref _handle)

	dllCall_cdecl _handle, sqlCloseConnection()
	dllFree _handle
	_handle = 0

endsub 

//-7-//
Sub DB_GetLastError(ref _handle, ref _error) 
   
	_error = ""
	dllCall_cdecl _handle, sqlGetLastErrorString(ref _error)

endsub 

//-8-//
Sub DB_GetSingleRowResult(ref _handle, ref _statement, ref _result)

	dllCall_cdecl _handle, sqlGetRecordSet(ref _statement)
	_result = ""
	
	var error : A255
	call DB_GetLastError(_handle, error)
	if(error = "")	
		dllCall_cdecl _handle, sqlGetFirst(ref _result)
	else
		exitwitherror error
	endif

endsub 

//-9-//
Sub DB_GetRecordSet(ref _handle, ref _statement)

	dllCall_cdecl _handle, sqlGetRecordSet(ref _statement)

endsub 

//-10-//
sub DB_GetNext(ref _handle, ref _result)

	_result = ""
	dllCall_cdecl _handle, sqlGetNext(ref _result)

endsub

sub Log(ref _message)
	var filename : A50 = "tipping_SIM.log"
	call DebugLog(_message)
	call LOG_ToFile(filename, _message)
endsub

sub DebugLog(ref _message)
	if(DebugLogMode = "T")
		var filename : A50 = "tipping_SIM_Debug.log"
		call LOG_ToFile(filename, _message)
	endif
endsub

sub LOG_ToFile(ref _filename, ref _message)
	var file : N5
	var dateTime : A50
	Fopen file, _filename, append
	
	format dateTime as @DAY, "/", @MONTH, "/", @YEAR, " - " , @HOUR, ":", @MINUTE, ":", @SECOND
	
	if(file = 0)
		errormessage @file_errstr
		exitcontinue
	endif
	
	Fwrite file, dateTime, " - ", _message
	Fclose file
endsub

