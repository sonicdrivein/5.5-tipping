--Update Cash and Credit Card total verbiage
update 
    micros.cm_item_def
set 
    cm_item_name = 'Cash Due'
where 
    cm_item_name = 'Cash' and 
    cm_item_type = 3;

update 
    micros.cm_item_def
set 
    cm_item_name = 'Credit Card Sales'
where 
    cm_item_name = 'Credit Cards' and 
    cm_item_type = 3;

--If total for tips doesn't exist, create it, then create the Tips Paid link
if not exists (select * from micros.cm_item_def where cm_item_name = 'Charge Tips Earned') then
	insert into 
 	      micros.cm_item_def (cm_item_name, cm_item_type)
	values 
        ('Charge Tips Earned', 3);

	insert into 
	        micros.cm_calc_item_dtl
	values 
        ((select cm_item_seq from micros.cm_item_def where cm_item_name = 'Charge Tips Earned' and cm_item_type = 3),
		(select cm_item_seq from micros.cm_item_def where res_item_id = (select tmed_seq from micros.tmed_def where obj_num = 106) and cm_item_type = 0),
		0);
end if;

--For non-payroll option, add the Tips Paid link to the Cash total, with a decuction operation.
if not exists (select * from micros.cm_calc_item_dtl where cm_item_seq = (select cm_item_seq from micros.cm_item_def where cm_item_name = 'Cash Due' and cm_item_type = 3) and 
	component_item_seq = (select cm_item_seq from micros.cm_item_def where res_item_id = (select tmed_seq from micros.tmed_def where obj_num = 106) and cm_item_type = 0)) then

	insert into 
		micros.cm_calc_item_dtl
	values ((select cm_item_seq from micros.cm_item_def where cm_item_name = 'Cash Due' and cm_item_type = 3),
		(select cm_item_seq from micros.cm_item_def where res_item_id = (select tmed_seq from micros.tmed_def where obj_num = 106) and cm_item_type = 0),
		1);
else
	update 
		micros.cm_calc_item_dtl
	set
		item_operation = 1
	where
		cm_item_seq = (select cm_item_seq from micros.cm_item_def where cm_item_name = 'Cash Due' and cm_item_type = 3) and 
		component_item_seq = (select cm_item_seq from micros.cm_item_def where res_item_id = (select tmed_seq from micros.tmed_def where obj_num = 106) and cm_item_type = 0);
end if;