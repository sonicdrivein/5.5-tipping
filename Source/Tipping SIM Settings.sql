--POS Configurator Settings for Tipping SIM

--Add interface
	--Object Number 40
	--Name Mobile Tipping
	
insert into micros.interface_def (interface_seq, obj_num, name)
values((select max(interface_seq) + 1 from micros.interface_def),40,'Mobile Tipping');	


--Add tender media
	--Object Number 106
	--Name Mobile Tips Paid
	--Tender Tab - Declare Tips Paid

insert into micros.tmed_def (tmed_seq, obj_num, name, type, ob_tdec19_tips_paid)
values((select max(tmed_seq) + 1 from micros.tmed_def), 106, 'Mobile Tips Paid','P', 'T');

--Add service charge
	--Object Number 501
	--Name Mobile Tip
	--Options Tab - Amount
	--Service Charge Tab - Post to Tips Paid Total
	--Service Charge Tab - Post to Charged Tips Total
	--Service Charge Tab - Tender/Media for Tips Paid
	
insert into micros.dsvc_def (dsvc_seq, obj_num, name, type, ob_dsvc03_amt, ob_svc17_chg_tip, ob_svc18_add_to_tips_paid, tips_paid_tmed_seq)
values((select max(dsvc_seq) + 1 from micros.dsvc_def), 501, 'Mobile Tip','S', 'T', 'T', 'T', (select tmed_seq from micros.tmed_def where obj_num = 501));

--Add tracking total
	--Number 20
	--Name -Tips Paid
	--Type - Service Charge
	--Total Number - 501 (Mobile Tip)

update micros.trk_ttl_def set ttl_type = 4, name = '-Tips Paid', ttl_num = (select dsvc_seq from micros.dsvc_def where obj_num = 501) where trk_ttl_seq = 20;

--Update Receipt line for service charges
	--RVC Printing
	--Other Service Charge Name - CC Tip
update micros.rvc_def set other_svc_chg_name = 'CC Tip';

	