# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo contains the SIM for correcting Order Ahead tips on 5.5, and a script to change settings in the system.

Once installed, the FINAL_TENDER event is enabled, which does the following:

For order type 6 checks, looks for the tip amount (using micros.dsvc_dtl and micros.dtl).
If a tip amount exists, updates the tracking totals for the API employee and carhop.
	Updates the micros.shift_emp_trk_ttl_temp and shift_emp_trk_ttl tables by removing the amount and count of the tip from the API employee, and adding the same to the carhop.
		If the carhop does not yet have tips, a row is inserted into micros.shift_emp_trk_ttl for the assigned tracking total.
It a tip amount exists, updates the Cash Management entries from the API employee to the carhop.
	Updates the rows for the tip service charge and tender in micros.cm_transaction_dtl to change the receptacle (Server Bank) from the API employee to the carhop.
	If the carhop does not yet have a Server Bank, a new row is inserted into micros.cm_receptable_dtl, and an assignment in micros.cm_employee_receptacle_assignment_dtl
Calls a posting stored procedure (micros.sp_postall)


### How do I get set up? ###

The isl file needs to be placed in the D:\Micros\Res\Pos\Etc folder, as well as the CAL folder (D:\Micros\Res\CAL\%Workstation Type%\Files\C(or CF)\Micros\Res\Pos\Etc)
The sql script needs to be run against the database in DBISQL

The SIM needs a database user and password entered (DB_USER and DB_PASSWORD at the top of the SIM).

When adding an order, the connector or test app needs to use Service Charge object # 201 to reflect the expected service charge type.

### How do I test this? ###
The executable in the Test App folder will add an order via the ResAPI (in the same manner as the connector), with a Burger, Fries and a 1.01 Tip on the API employee.
Once the SIM and sql file are installed, the app can be used to create an order, which upon hop will trigger the SIM.

The SIM features logging, as well as full debug logging (that provides every sql command run and the result). The files are in D:\micros\res\pos\etc\tipping_SIM.log and tipping_SIM_Debug.log
The debugging log can be disabled by modifying the configuration at the top of the SIM (Setting debugMode to "F")




